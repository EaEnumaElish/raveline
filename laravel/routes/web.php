<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\MainController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


//Route::get('/', action::'MainController@home');
Route::get('/', [MainController::class,'home']);
Route::get('/films', [MainController::class,'films']);
Route::get('/social', [MainController::class,'social']);