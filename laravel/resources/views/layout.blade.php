<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>@yield('title')</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
</head>
<body class="">
    @include('layouts.nav')
    <br><br>
    <div class="conteiner">
        @yield('main_content')
    </div>
    <br><br>
    @include('layouts.footer')
</body>
</html>