<header>
        <nav class="navbar navbar-expand-md navbar-dark  fixed-top bg-dark">
            <a class="navbar-brand" href="/">Raveline</a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarCollapse">
                <ul class="navbar-nav mr-auto">
                    <li class="nav-item active">
                    <a class="nav-link" href="/">Домашняя<span class="sr-only">(current)</span></a>
                    </li>
                    <li class="nav-item">
                    <a class="nav-link" href="/films">Фильмы</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="/social">Социальная сеть</a>
                    </li>
                </ul>
                
                <form class="form-inline mt-2 mt-md-0" action="/">
                    <input class="form-control mr-sm-2" type="text" placeholder="Поиск" aria-label="Search">
                </form>
                <form class="form-inline mt-2 mt-md-0" action="/">
                <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Войти</button>
                </form>

            </div>
        </nav>
    </header>